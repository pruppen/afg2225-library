import sys
from PyQt5.QtWidgets import (QLineEdit, QPushButton, QApplication,
    QVBoxLayout, QDialog)
import pyvisa
from PyQt5 import QtTest
import AFG2225


class Form(QDialog):

    def __init__(self, parent=None):
        super(Form, self).__init__(parent)
        self.status = 'Off'  # output on or off
        # Create widgets
        self.edit_freq = QLineEdit("Set frequency in kHz")
        self.edit_amp = QLineEdit("Set amplitude in V")
        self.button_set = QPushButton("Change Waveform")
        self.button_on = QPushButton("Start")
        # Create layout and add widgets
        layout = QVBoxLayout()
        layout.addWidget(self.edit_freq)
        layout.addWidget(self.edit_amp)
        layout.addWidget(self.button_set)
        layout.addWidget(self.button_on)
        # Set dialog layout
        self.setLayout(layout)
        # Add button signal to greetings slot
        self.button_set.clicked.connect(self.update_button_set)
        self.button_on.clicked.connect(self.setOnOff)

    def update_button_set(self):
        my_instrument.set_frequency(self.edit_freq.text(), "MHz")
        QtTest.QTest.qWait(50) #need to wait for short period in order to be ready for next command
        my_instrument.set_amplitude(self.edit_amp.text())

    def setOnOff(self):
        if self.status == 'Off':
            my_instrument.turn_on()
            self.button_on.setText('Stop')
            self.status = 'On'
        else:
            my_instrument.turn_off()
            self.button_on.setText('Start')
            self.status = 'Off'


if __name__ == '__main__':
    # Create the Qt Application
    app = QApplication(sys.argv)
    # Create and show the form
    form = Form()

    form.show()
    try:

        # Connect to the Function Generator (connection = COM port e.g. COM1 also possible)
        my_instrument = AFG2225.AFG2225(connection='ASRL6::INSTR')
        # Run the main Qt loop
        sys.exit(app.exec_())

    except:
        print("No device connected!")
