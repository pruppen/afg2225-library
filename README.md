# AFG2225 library

Python library for communication with GW Instek AFG-2225 function generator.



Installation:

1) Download AFG-2225 USB driver from GW Instek Website:
https://www.gwinstek.com/en-global/products/detail/AFG-2225

2) Download NI-VISA:
http://www.ni.com/en-za/support/downloads/drivers/download.ni-visa.html#329456

3) Install pyvisa python library:
$pip install pyvisa

